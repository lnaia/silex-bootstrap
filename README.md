Simple-Bootstrap
===============

# Silex #
http://silex.sensiolabs.org/

# Twitter Bootstrap #
http://getbootstrap.com/components/


This simple bootstrap, aims to fast prototype interfaces or even small a api.

From here, its easy to start using bootstrap, compass or angular.


## Install with composer ##
```
$ php composer.phar install
```

